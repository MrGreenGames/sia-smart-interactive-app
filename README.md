# SIA
Programmed in Python 2.7 and converted to 3.6 and 3.7

The Smart Interactive App is used to make the user's life much more easier. With its many functions it can be a calculator, email sender, browse the web, and much more! Plus it has a builtin helper called Shelly(SIAHelp). It also has an excellent security system that calls protocols as soon as it detects a security breach.

# Downloads
Python 3.6: https://www.python.org/downloads/release/python-368/

Python 3.7: https://www.python.org/downloads/release/python-373/

Windows download:
https://bit.ly/2JYMRXr

Mac OS X download:
https://bit.ly/2MGML8S
